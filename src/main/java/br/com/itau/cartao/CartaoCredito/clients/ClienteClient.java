package br.com.itau.cartao.CartaoCredito.clients;

import br.com.itau.cartao.CartaoCredito.clients.errordecoder.ClienteClientConfiguration;
import br.com.itau.cartao.CartaoCredito.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/v1/clientes/{id}")
    Cliente consultarClientePorId(@PathVariable Long id);

}
