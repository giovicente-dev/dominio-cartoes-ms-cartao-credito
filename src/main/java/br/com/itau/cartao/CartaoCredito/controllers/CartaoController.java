package br.com.itau.cartao.CartaoCredito.controllers;

import br.com.itau.cartao.CartaoCredito.dtos.CartaoDTOEntradaPatch;
import br.com.itau.cartao.CartaoCredito.dtos.CartaoDTOEntradaPost;
import br.com.itau.cartao.CartaoCredito.models.Cartao;
import br.com.itau.cartao.CartaoCredito.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<Cartao> postCartao(@RequestBody CartaoDTOEntradaPost cartaoDTOEntradaPost) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoDTOEntradaPost.getNumero());
        cartao.setClienteId(cartaoDTOEntradaPost.getClienteId());
        cartao.setAtivo(false);

        Cartao cartaoObjeto = cartaoService.salvarCartao(cartao);

        return ResponseEntity.status(201).body(cartaoObjeto);
    }

    @PatchMapping("/{numero}")
    public Cartao patchAtivarCartao(@PathVariable(name = "numero") String numero, @RequestBody CartaoDTOEntradaPatch cartaoDTOEntradaPatch) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(cartaoDTOEntradaPatch.isAtivo());

        try {
            Cartao cartaoObjeto = cartaoService.ativarCartao(numero, cartao);
            return cartaoObjeto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public Cartao getCartaoByNumero(@PathVariable(name = "numero") String numero) {
        try {
            Cartao cartaoObjeto = cartaoService.consultarPorNumero(numero);
            return cartaoObjeto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
