package br.com.itau.cartao.CartaoCredito.services;

import br.com.itau.cartao.CartaoCredito.clients.ClienteClient;
import br.com.itau.cartao.CartaoCredito.models.Cartao;
import br.com.itau.cartao.CartaoCredito.models.Cliente;
import br.com.itau.cartao.CartaoCredito.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao salvarCartao(Cartao cartao) {
        Cliente clienteObjeto = clienteClient.consultarClientePorId(cartao.getClienteId());
        Cartao cartaoObjeto = cartaoRepository.save(cartao);

        return cartaoObjeto;
    }

    public Cartao ativarCartao(String numero, Cartao cartao) {
        if (cartaoRepository.existsByNumero(numero)) {
            Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
            cartao.setId(cartaoOptional.get().getId());
            cartao.setNumero(numero);
            cartao.setClienteId(cartaoOptional.get().getClienteId());

            Cartao cartaoObjeto = salvarCartao(cartao);

            return cartaoObjeto;
        }

        throw new RuntimeException("Cartão não encontrado.");
    }

    public Cartao consultarPorNumero(String numero) {

        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        Cliente clienteObjeto = clienteClient.consultarClientePorId(cartaoOptional.get().getClienteId());

        if (cartaoOptional.isPresent()) {
            return cartaoOptional.get();
        }

        throw new RuntimeException("Cartão não encontrado.");
    }

}