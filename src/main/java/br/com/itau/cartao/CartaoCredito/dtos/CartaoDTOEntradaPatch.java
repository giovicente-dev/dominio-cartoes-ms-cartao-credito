package br.com.itau.cartao.CartaoCredito.dtos;

public class CartaoDTOEntradaPatch {

    private boolean ativo;

    public CartaoDTOEntradaPatch() { }

    public boolean isAtivo() { return ativo; }

    public void setAtivo(boolean ativo) { this.ativo = ativo; }

}
