package br.com.itau.cartao.CartaoCredito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@RibbonClients(defaultConfiguration = RibbonAutoConfiguration.class)
public class CartaoCreditoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartaoCreditoApplication.class, args);
	}

}
